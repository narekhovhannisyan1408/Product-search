import { ProductSearchPage } from './app.po';

describe('product-search App', () => {
  let page: ProductSearchPage;

  beforeEach(() => {
    page = new ProductSearchPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
