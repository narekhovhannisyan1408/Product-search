import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'search',
    pure: false
})
export class SearchPipe implements PipeTransform {

    transform(data: any[], searchTerm: string): any[] {
        if (searchTerm){
            searchTerm = searchTerm.toLowerCase();
            return data.filter(item => {
                return item.toLowerCase().indexOf(searchTerm) !== -1
            });
        }
    }

}
@Pipe({
    name: 'searchDesc',
    pure: false
})
export class SearchPipeDesc implements PipeTransform {
    transform(items:any[], args:string[]):any[] {
        if (typeof items === 'object') {
            let resultArray = [];

            if (args && args.length === 0) {
                resultArray = items;
            }

            else {
                for (let item of items) {
                    for (let desc of item.description) {
                        if (desc != null && desc.match(new RegExp(''+args, 'i'))) {
                            resultArray.push(item);
                            break
                        }
                    }
                }
            }
            return resultArray;
        }
        else {
            return null;
        }
    }
}