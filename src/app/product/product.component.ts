import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

    cat:string;
    name:string;

  constructor() {}

  ngOnInit() {
      this.cat = sessionStorage.getItem('cat');
      this.name = sessionStorage.getItem('name');
  }

}
