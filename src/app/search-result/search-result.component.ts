import {Component, OnInit, Input} from '@angular/core';
import {CallSvcService} from '../service/call-svc.service';

@Component({
    selector: 'app-search-result',
    templateUrl: './search-result.component.html',
    styleUrls: ['./search-result.component.scss'],
    providers: [CallSvcService]
})
export class SearchResultComponent implements OnInit {

    items = [];

    @Input() searchValue: string;

    constructor(private  callService: CallSvcService) {
        callService.getCall().subscribe(data => {
            this.items = data;
        });
    }
    marker = function (val , searchVal) {
        return val.toLowerCase().replace(searchVal.toLowerCase(),"<b>" + searchVal.toLowerCase() + "</b>")
    };

    sendData = function (categories, name) {
        sessionStorage.setItem('cat', categories);
        sessionStorage.setItem('name', name);
    };

    ngOnInit() {

    }

}
