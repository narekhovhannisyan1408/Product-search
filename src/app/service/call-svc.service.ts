import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CallSvcService {

    constructor (
        private http: Http
    ) {}

      private callUrl = "assets/db.json";


    getCall() {
        return this.http.get(this.callUrl)
            .map((res:Response) => res.json());
    }

}