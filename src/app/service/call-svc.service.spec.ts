import { TestBed, inject } from '@angular/core/testing';

import { CallSvcService } from './call-svc.service';

describe('CallSvcService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CallSvcService]
    });
  });

  it('should be created', inject([CallSvcService], (service: CallSvcService) => {
    expect(service).toBeTruthy();
  }));
});
